package org.isj.hawa.groupe1.dto;

public class UECreateDto {
    private String nom;
    private String CodeUE;

    public UECreateDto() {
    }

    public UECreateDto(String nom, String codeUE) {
        this.nom = nom;
        CodeUE = codeUE;
    }

    public UECreateDto(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCodeUE() {
        return CodeUE;
    }

    public void setCodeUE(String codeUE) {
        CodeUE = codeUE;
    }

}
