package org.isj.hawa.groupe1.dto;


import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public class SceanceUpdateDto {
    private Long id;
    private LocalDate dateSceance;
    private LocalTime heurDebutPrevu;
    private LocalTime heurFinPrevu;
    private String mailEnseignant;
    private Long idUE;
    private Long idClasses;

    public SceanceUpdateDto() {
    }


    public SceanceUpdateDto(Long id, LocalDate dateSceance, LocalTime heurDebutPrevu,
                            LocalTime heurFinPrevu, String mailEnseignant, Long idUE, Long idClasses) {
        this.id = id;
        this.dateSceance = dateSceance;
        this.heurDebutPrevu = heurDebutPrevu;
        this.heurFinPrevu = heurFinPrevu;
        this.mailEnseignant = mailEnseignant;
        this.idUE = idUE;
        this.idClasses = idClasses;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateSceance() {
        return dateSceance;
    }

    public void setDateSceance(LocalDate dateSceance) {
        this.dateSceance = dateSceance;
    }

    public LocalTime getHeurDebutPrevu() {
        return heurDebutPrevu;
    }

    public void setHeurDebutPrevu(LocalTime heurDebutPrevu) {
        this.heurDebutPrevu = heurDebutPrevu;
    }

    public LocalTime getHeurFinPrevu() {
        return heurFinPrevu;
    }

    public void setHeurFinPrevu(LocalTime heurFinPrevu) {
        this.heurFinPrevu = heurFinPrevu;
    }

    public String getMailEnseignant() {
        return mailEnseignant;
    }

    public void setMailEnseignant(String mailEnseignant) {
        this.mailEnseignant = mailEnseignant;
    }

    public Long getIdUE() {
        return idUE;
    }

    public void setIdUE(Long idUE) {
        this.idUE = idUE;
    }

    public Long getIdClasses() {
        return idClasses;
    }

    public void setIdClasses(Long idClasses) {
        this.idClasses = idClasses;
    }
}

