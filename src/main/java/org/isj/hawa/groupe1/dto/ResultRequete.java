package org.isj.hawa.groupe1.dto;


import java.time.LocalDate;

public interface ResultRequete {
    String getNom();
    String getGrade();
    String getEmail();
    String getClasse();
    LocalDate getDate_sceance();
    String getTotalheur();
    String getSemaine();
}
