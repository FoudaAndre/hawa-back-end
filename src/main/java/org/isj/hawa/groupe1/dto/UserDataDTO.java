package org.isj.hawa.groupe1.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


public class UserDataDTO {

  @ApiModelProperty(position = 0)
  private String nom;

  @ApiModelProperty(position = 1)
  private String prenom;

  @ApiModelProperty(position = 2)
  private String email;

  @ApiModelProperty(position = 3)
  private String telephone;

  @ApiModelProperty(position = 4)
  private String grade;

  @ApiModelProperty(position = 5)
  private String password;

  public UserDataDTO(String nom, String prenom, String email, String telephone, String grade, String password) {
    this.nom = nom;
    this.prenom = prenom;
    this.email = email;
    this.telephone = telephone;
    this.grade = grade;
    this.password = password;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getPrenom() {
    return prenom;
  }

  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getTelephone() {
    return telephone;
  }

  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getGrade() {
    return grade;
  }

  public void setGrade(String grade) {
    this.grade = grade;
  }
}
