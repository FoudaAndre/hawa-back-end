package org.isj.hawa.groupe1.dto;

import lombok.Getter;
import lombok.Setter;


public class UserUpdateDto {
    private String nom;
    private String prenom;
    private String email;
    private String telephone;
    private String grade;

    public UserUpdateDto() {
    }

    public UserUpdateDto(String nom, String prenom, String email, String telephone, String grade) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.telephone = telephone;
        this.grade = grade;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }
}
