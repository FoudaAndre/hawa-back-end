package org.isj.hawa.groupe1.dto;


import java.io.Serializable;
import java.time.LocalDate;

public class SemestreCreateDto implements Serializable {
    private String nomSemestre;
    private LocalDate dateDebut;
    private LocalDate dateFin;
    private Long idAnnee;

    public SemestreCreateDto() {
    }

    public SemestreCreateDto(String nomSemestre, LocalDate dateDebut, LocalDate dateFin, Long idAnnee) {
        this.nomSemestre = nomSemestre;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.idAnnee = idAnnee;
    }

    public String getNomSemestre() {
        return nomSemestre;
    }

    public void setNomSemestre(String nomSemestre) {
        this.nomSemestre = nomSemestre;
    }

    public LocalDate getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDate getDateFin() {
        return dateFin;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }

    public Long getIdAnnee() {
        return idAnnee;
    }

    public void setIdAnnee(Long idAnnee) {
        this.idAnnee = idAnnee;
    }


}
