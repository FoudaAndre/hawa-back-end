package org.isj.hawa.groupe1.dto;

import java.time.LocalDate;


public class AnneeAcademiqueUpdateDto {
    private long id;
    private String nomAnnee;
    private LocalDate dateDebut;
    private LocalDate dateFin;

    public AnneeAcademiqueUpdateDto(long id, String nomAnnee, LocalDate dateDebut, LocalDate dateFin) {
        this.id = id;
        this.nomAnnee = nomAnnee;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
    }

    public AnneeAcademiqueUpdateDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNomAnnee() {
        return nomAnnee;
    }

    public void setNomAnnee(String nomAnnee) {
        this.nomAnnee = nomAnnee;
    }

    public LocalDate getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDate getDateFin() {
        return dateFin;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }
}
