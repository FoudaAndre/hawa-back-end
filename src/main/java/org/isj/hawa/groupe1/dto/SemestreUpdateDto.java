package org.isj.hawa.groupe1.dto;

import java.time.LocalDate;

public class SemestreUpdateDto {
    private long id;
    private String nomSemestre;
    private LocalDate dateDebut;
    private LocalDate dateFin;

    public SemestreUpdateDto(long id, String nomSemestre, LocalDate dateDebut, LocalDate dateFin) {
        this.id = id;
        this.nomSemestre = nomSemestre;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
    }

    public SemestreUpdateDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNomSemestre() {
        return nomSemestre;
    }

    public void setNomSemestre(String nomSemestre) {
        this.nomSemestre = nomSemestre;
    }

    public LocalDate getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDate getDateFin() {
        return dateFin;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }
}
