package org.isj.hawa.groupe1.dto;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.isj.hawa.groupe1.model.Role;

@Getter
@Setter
public class UserResponseDTO {

  @ApiModelProperty(position = 0)
  private String nom;

  @ApiModelProperty(position = 1)
  private String prenom;

  @ApiModelProperty(position = 2)
  private String email;

  @ApiModelProperty(position = 3)
  private String telephone;

  @ApiModelProperty(position = 4)
  private String grade;

  @ApiModelProperty(position = 5)
  List<Role> roles;
}
