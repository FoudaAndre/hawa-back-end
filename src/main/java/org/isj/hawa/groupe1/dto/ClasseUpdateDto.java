package org.isj.hawa.groupe1.dto;

public class ClasseUpdateDto {
    private long Id;
    private String filier;
    private String specialite;
    private int niveau;

    public ClasseUpdateDto() {
    }

    public ClasseUpdateDto(long id, String filier, String specialite, int niveau) {
        Id = id;
        this.filier = filier;
        this.specialite = specialite;
        this.niveau = niveau;
    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getFilier() {
        return filier;
    }

    public void setFilier(String filier) {
        this.filier = filier;
    }

    public String getSpecialite() {
        return specialite;
    }

    public void setSpecialite(String specialite) {
        this.specialite = specialite;
    }

    public int getNiveau() {
        return niveau;
    }

    public void setNiveau(int niveau) {
        this.niveau = niveau;
    }
}
