package org.isj.hawa.groupe1.dto;

public class UEUpdateDto {
    private long id;
    private String nom;
    private String CodeUE;

    public UEUpdateDto(long id, String nom, String codeUE) {
        this.id = id;
        this.nom = nom;
        CodeUE = codeUE;
    }

    public UEUpdateDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCodeUE() {
        return CodeUE;
    }

    public void setCodeUE(String codeUE) {
        CodeUE = codeUE;
    }
}
