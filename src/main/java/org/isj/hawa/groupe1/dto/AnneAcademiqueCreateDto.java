package org.isj.hawa.groupe1.dto;



import java.io.Serializable;
import java.time.LocalDate;

public class AnneAcademiqueCreateDto implements Serializable {
    private String nomAnnee;
    private LocalDate dateDebut;
    private LocalDate dateFin;

    public AnneAcademiqueCreateDto() {
    }

    public AnneAcademiqueCreateDto(String nomAnnee, LocalDate dateDebut, LocalDate dateFin) {
        this.nomAnnee = nomAnnee;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
    }

    public String getNomAnnee() {
        return nomAnnee;
    }

    public void setNomAnnee(String nomAnnee) {
        this.nomAnnee = nomAnnee;
    }

    public LocalDate getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDate getDateFin() {
        return dateFin;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }
}
