package org.isj.hawa.groupe1.dto;

public class DeleguerCreateDto {
    private String nom;

    private String prenom;

    private String email;

    private String telephone;

    private String grade;

    private String matricule;
}
