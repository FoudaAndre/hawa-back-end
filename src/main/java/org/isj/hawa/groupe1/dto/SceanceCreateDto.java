package org.isj.hawa.groupe1.dto;


import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public class SceanceCreateDto {
    private LocalDate dateSceance;
    private LocalTime heurDebutPrevu;
    private LocalTime heurFinPrevu;
    private String mailEnseignant;
    private Long idUE;
    private Long idClasses;

    public SceanceCreateDto() {
    }

    public SceanceCreateDto(LocalDate dateSceance, LocalTime heurDebutPrevu, LocalTime heurFinPrevu, String mailEnseignant, Long idUE, Long idClasses) {
        this.dateSceance = dateSceance;
        this.heurDebutPrevu = heurDebutPrevu;
        this.heurFinPrevu = heurFinPrevu;
        this.mailEnseignant = mailEnseignant;
        this.idUE = idUE;
        this.idClasses = idClasses;
    }

    public LocalDate getDateSceance() {
        return dateSceance;
    }

    public void setDateSceance(LocalDate dateSceance) {
        this.dateSceance = dateSceance;
    }

    public LocalTime getHeurDebutPrevu() {
        return heurDebutPrevu;
    }

    public void setHeurDebutPrevu(LocalTime heurDebutPrevu) {
        this.heurDebutPrevu = heurDebutPrevu;
    }

    public LocalTime getHeurFinPrevu() {
        return heurFinPrevu;
    }

    public void setHeurFinPrevu(LocalTime heurFinPrevu) {
        this.heurFinPrevu = heurFinPrevu;
    }

    public String getMailEnseignant() {
        return mailEnseignant;
    }

    public void setMailEnseignant(String mailEnseignant) {
        this.mailEnseignant = mailEnseignant;
    }

    public Long getIdUE() {
        return idUE;
    }

    public void setIdUE(Long idUE) {
        this.idUE = idUE;
    }

    public Long getIdClasses() {
        return idClasses;
    }

    public void setIdClasses(Long idClasses) {
        this.idClasses = idClasses;
    }
}
