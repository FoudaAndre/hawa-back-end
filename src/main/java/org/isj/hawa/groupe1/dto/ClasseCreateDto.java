package org.isj.hawa.groupe1.dto;

public class ClasseCreateDto {
    private String filier;
    private String specialite;
    private int niveau;

    public ClasseCreateDto() {
    }

    public ClasseCreateDto(String filier, String specialite, int niveau) {
        this.filier = filier;
        this.specialite = specialite;
        this.niveau = niveau;
    }

    public String getFilier() {
        return filier;
    }

    public void setFilier(String filier) {
        this.filier = filier;
    }

    public String getSpecialite() {
        return specialite;
    }

    public void setSpecialite(String specialite) {
        this.specialite = specialite;
    }

    public int getNiveau() {
        return niveau;
    }

    public void setNiveau(int niveau) {
        this.niveau = niveau;
    }
}
