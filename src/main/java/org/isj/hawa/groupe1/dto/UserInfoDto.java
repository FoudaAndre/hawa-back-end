package org.isj.hawa.groupe1.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserInfoDto {
    private String nom;
    private String prenom;
    private String email;
    private String telephone;
    private String grade;
}
