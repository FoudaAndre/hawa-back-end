package org.isj.hawa.groupe1;


import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import org.isj.hawa.groupe1.service.UserService;

@SpringBootApplication
public class Hawa implements CommandLineRunner {

  @Autowired
  UserService userService;

  public static void main(String[] args) {
    SpringApplication.run(Hawa.class, args);
  }

  @Bean
  public ModelMapper modelMapper() {
    return new ModelMapper();
  }

  @Override
  public void run(String... params) throws Exception {
  /*
    Admin admin = new Admin();
    admin.setLogin("admin");
    admin.setMdp("admin");
    admin.setEmail("admin@email.com");
    admin.setNom("Ndoumou Fouda Mballa");
    admin.setPrenom("André");
    admin.setDateN(new SimpleDateFormat("yyyy-MM-dd").parse("26/03/1996"));
    admin.setTelephone("+237651069554");

    userService.signupAdmin(admin);

    User client = new User();
    client.setLogin("client");
    client.setMdp("client");
    client.setEmail("client@email.com");
    client.setNom("BINELI OMBA ");
    client.setPrenom("Audrey");
    client.setDateN(new SimpleDateFormat("yyyy-MM-dd").parse("26/03/1996"));
    client.setTelephone("+237651069554");
    userService.signupuser(client);
*/
  }
}
