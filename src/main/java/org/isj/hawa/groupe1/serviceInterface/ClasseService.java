package org.isj.hawa.groupe1.serviceInterface;



import org.isj.hawa.groupe1.dto.ClasseCreateDto;
import org.isj.hawa.groupe1.dto.ClasseUpdateDto;
import org.isj.hawa.groupe1.model.Classe;

import java.util.List;

public interface ClasseService {
    ClasseUpdateDto creerClasse(ClasseCreateDto dto);
    List<ClasseUpdateDto> ListesClass();
    ClasseUpdateDto updateClass(ClasseUpdateDto dto);
    Classe findbyId(Long id);
}
