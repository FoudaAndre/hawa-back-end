package org.isj.hawa.groupe1.serviceInterface;

import org.isj.hawa.groupe1.dto.SceanceCreateDto;
import org.isj.hawa.groupe1.dto.SceanceUpdateDto;

import java.util.List;

public interface SceanceService {
    SceanceUpdateDto CreerSceance(SceanceCreateDto dto);
    SceanceUpdateDto updateSceance(SceanceUpdateDto dto);
    List<SceanceUpdateDto> listSceance();
    void startClasse(Long id);
    void endClasse(Long id);
}
