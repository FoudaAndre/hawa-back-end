package org.isj.hawa.groupe1.serviceInterface;



import org.isj.hawa.groupe1.dto.AnneAcademiqueCreateDto;
import org.isj.hawa.groupe1.dto.AnneeAcademiqueUpdateDto;
import org.isj.hawa.groupe1.model.AnneeAcademique;

import java.util.List;

public interface AnneAcademiqueService<Liste> {
 AnneeAcademiqueUpdateDto anneeCreate(AnneAcademiqueCreateDto dto);
 AnneeAcademiqueUpdateDto anneeUpdate(AnneeAcademiqueUpdateDto annee);
 List<AnneeAcademiqueUpdateDto> ListeAnnee();
}

