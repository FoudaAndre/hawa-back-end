package org.isj.hawa.groupe1.serviceInterface;



import org.isj.hawa.groupe1.dto.UECreateDto;
import org.isj.hawa.groupe1.dto.UEUpdateDto;

import java.util.List;

public interface UEService {
 UEUpdateDto creerUE(UECreateDto dto);
 UEUpdateDto updateUE(UEUpdateDto dto);
 List<UEUpdateDto> listUE();
}
