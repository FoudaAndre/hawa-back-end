package org.isj.hawa.groupe1.serviceInterface;


import org.isj.hawa.groupe1.dto.SemestreCreateDto;
import org.isj.hawa.groupe1.dto.SemestreUpdateDto;
import org.isj.hawa.groupe1.model.Semestre;

import java.util.List;

public interface SemestreService {
    SemestreUpdateDto semestrecreate(SemestreCreateDto dto);
    SemestreUpdateDto semestreUpdate(SemestreUpdateDto dto);
    List<Semestre> findsemestre();
    Semestre getSemestre(Long id);
}
