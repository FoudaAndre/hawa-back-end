package org.isj.hawa.groupe1.controller;

import org.isj.hawa.groupe1.dto.UECreateDto;
import org.isj.hawa.groupe1.dto.UEUpdateDto;
import org.isj.hawa.groupe1.serviceInterface.UEService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/ue")
public class UEController {

    @Autowired
    private UEService service;

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_DELEGUER')" )
    @PostMapping("/add")
    public UEUpdateDto creer(@RequestBody UECreateDto dto){
        return service.creerUE(dto);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_DELEGUER') or hasRole('ROLE_ENSEIGNANT')" )
    @GetMapping
    public List<UEUpdateDto> list(){
        return service.listUE();
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_DELEGUER')" )
    @PutMapping("/update")
    public UEUpdateDto update(@RequestBody UEUpdateDto dto){
        return service.updateUE(dto);
    }
}
