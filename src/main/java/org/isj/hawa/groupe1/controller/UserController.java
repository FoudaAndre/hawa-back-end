package org.isj.hawa.groupe1.controller;

import javax.servlet.http.HttpServletRequest;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.isj.hawa.groupe1.dto.*;
import org.isj.hawa.groupe1.model.User;
import org.isj.hawa.groupe1.service.UserService;

import org.json.JSONObject;

import java.util.List;

@RestController
@RequestMapping("/users")
@Api(tags = "users")
public class UserController {

  @Autowired
  private UserService userService;

  @Autowired
  private ModelMapper modelMapper;

  @PostMapping("/signupadmin")
  @ApiOperation(value = "${UserController.signup}")
  @ApiResponses(value = {//
      @ApiResponse(code = 400, message = "Something went wrong"), //
      @ApiResponse(code = 403, message = "Access denied"), //
      @ApiResponse(code = 422, message = "Username is already in use")})
  public String signupAdmin(@ApiParam("Signup User") @RequestBody UserDataDTO user) {
    JSONObject json = new JSONObject();
    json.put("token",userService.signupAdmin(modelMapper.map(user, User.class)));
    return json.toString();
  }

  @PostMapping("/signUpEnseignient")
  @ApiOperation(value = "${UserController.signup}")
  @ApiResponses(value = {//
          @ApiResponse(code = 400, message = "Something went wrong"), //
          @ApiResponse(code = 403, message = "Access denied"), //
          @ApiResponse(code = 422, message = "Username is already in use")})
  public String signupEnsegnient(@ApiParam("Signup User") @RequestBody UserDataDTO user) {
    JSONObject json = new JSONObject();
    json.put("token",userService.signUpEnseignant(modelMapper.map(user, User.class)));
    return json.toString();
  }

  @PostMapping("/signUpDeleguer")
  @ApiOperation(value = "${UserController.signup}")
  @ApiResponses(value = {//
          @ApiResponse(code = 400, message = "Something went wrong"), //
          @ApiResponse(code = 403, message = "Access denied"), //
          @ApiResponse(code = 422, message = "Username is already in use")})
  public void signupDeleguer(@ApiParam("Signup User") @RequestBody DeleguerDataDTO user) {
    JSONObject json = new JSONObject();
    userService.signUpDeleguer(user);
  }

  @PostMapping("/signin")
  @ApiOperation(value = "${UserController.signin}")
  @ApiResponses(value = {//
          @ApiResponse(code = 400, message = "Something went wrong"), //
          @ApiResponse(code = 422, message = "Invalid username/password supplied")})
  public String login(@ApiParam("Signup Login") @RequestBody LoginDto login) {
    JSONObject json = new JSONObject();
    json.put("token",userService.signin(login.getEmail(), login.getPassword()));
    return json.toString();
  }

  @DeleteMapping(value = "/{username}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @ApiOperation(value = "${UserController.delete}", authorizations = { @Authorization(value="apiKey") })
  @ApiResponses(value = {//
          @ApiResponse(code = 400, message = "Something went wrong"), //
          @ApiResponse(code = 403, message = "Access denied"), //
          @ApiResponse(code = 404, message = "The user doesn't exist"), //
          @ApiResponse(code = 500, message = "Expired or invalid JWT token")})
  public String delete(@ApiParam("Username") @PathVariable String username) {
    userService.delete(username);
    return username;
  }

  @GetMapping(value = "Find/{username}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @ApiOperation(value = "${UserController.search}", response = UserInfoDto.class, authorizations = { @Authorization(value="apiKey") })
  @ApiResponses(value = {//
          @ApiResponse(code = 400, message = "Something went wrong"), //
          @ApiResponse(code = 403, message = "Access denied"), //
          @ApiResponse(code = 404, message = "The user doesn't exist"), //
          @ApiResponse(code = 500, message = "Expired or invalid JWT token")})
  public User search(@ApiParam("Username") @PathVariable String mail) {
    return userService.searchUser(mail);
  }

  @GetMapping(value = "/me")
  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
  @ApiOperation(value = "${UserController.me}")
  @ApiResponses(value = {//
          @ApiResponse(code = 400, message = "Something went wrong"), //
          @ApiResponse(code = 403, message = "Access denied"), //
          @ApiResponse(code = 500, message = "Expired or invalid JWT token")})
  public UserInfoDto whoami() {
    User usr= userService.whoami();
    return modelMapper.map(usr, UserInfoDto.class);
  }

  @GetMapping("/refresh")
  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
  public String refresh(HttpServletRequest req) {
    return userService.refresh(req.getRemoteUser());
  }

  @PostMapping("/changepwd")
  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
  @ApiOperation(value = "${UserController.signup}")
  @ApiResponses(value = {//
          @ApiResponse(code = 400, message = "Something went wrong"), //
          @ApiResponse(code = 403, message = "Access denied"), //
          @ApiResponse(code = 422, message = "Username is already in use")})
  public String changepassword(@ApiParam("changepwd passwordDto") @RequestBody ChangePasswordDTO password) {
    JSONObject json = new JSONObject();
    json.put("message",userService.passwordChange(password));
    return json.toString();
  }

  @PutMapping("/updateuser")
  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
  public void updateUser(@RequestBody UserUpdateDto userupdate){
    userService.updateUserInfo(userupdate);
  }

  @PutMapping("/list")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public List<UserUpdateDto> listUser(){
    return userService.listUtilisateur();
  }
}
