package org.isj.hawa.groupe1.controller;

import org.isj.hawa.groupe1.dto.SceanceCreateDto;
import org.isj.hawa.groupe1.dto.SceanceUpdateDto;
import org.isj.hawa.groupe1.serviceInterface.SceanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/sceance")
public class SceanceControlleur {
    @Autowired
    private SceanceService service;

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_DELEGUER')" )
    @PostMapping("/add")
    public SceanceUpdateDto creer(@RequestBody SceanceCreateDto dto){
        return service.CreerSceance(dto);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_DELEGUER') or hasRole('ROLE_ENSEIGNANT')" )
    @GetMapping("/list")
    public List<SceanceUpdateDto> list(){
        return service.listSceance();
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_DELEGUER')" )
    @PutMapping("/update")
    public SceanceUpdateDto updateSceance (SceanceUpdateDto dto){
        return service.updateSceance(dto);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_DELEGUER')" )
    @PutMapping("/start/{id}")
    public void startSceance (@RequestParam Long id){
        service.startClasse(id);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_DELEGUER')" )
    @PutMapping("/stop/{id}")
    public void stopSceance (@RequestParam Long id){
        service.endClasse(id);
    }
}
