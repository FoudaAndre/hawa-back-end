package org.isj.hawa.groupe1.controller;

import org.isj.hawa.groupe1.dto.ClasseCreateDto;
import org.isj.hawa.groupe1.dto.ClasseUpdateDto;
import org.isj.hawa.groupe1.model.Classe;
import org.isj.hawa.groupe1.serviceInterface.ClasseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/classe")
public class ClasseControlleur {

    @Autowired
    private ClasseService service;

    @PostMapping("/add")
    public ClasseUpdateDto addClasse(@RequestBody ClasseCreateDto dto){
        return service.creerClasse(dto);
    }

    @GetMapping("/list")
    public List<ClasseUpdateDto> listClasses(){
        return service.ListesClass();
    }

    @PutMapping("/update")
    public ClasseUpdateDto updateclasse(@RequestBody ClasseUpdateDto dto){
        return service.updateClass(dto);
    }

    @GetMapping("/get/{id}")
    public Classe find(@RequestParam Long id){
        return service.findbyId(id);
    }
}
