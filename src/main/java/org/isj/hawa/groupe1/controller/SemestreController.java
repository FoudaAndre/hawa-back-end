package org.isj.hawa.groupe1.controller;

import org.isj.hawa.groupe1.dto.SemestreCreateDto;
import org.isj.hawa.groupe1.dto.SemestreUpdateDto;
import org.isj.hawa.groupe1.model.Semestre;
import org.isj.hawa.groupe1.serviceInterface.SemestreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/semestre")
public class SemestreController {

    @Autowired
    private SemestreService service;

    @PreAuthorize("hasRole('ROLE_ADMIN')" )
    @PostMapping("/add")
    public SemestreUpdateDto creer(@RequestBody SemestreCreateDto dto){
        return service.semestrecreate(dto);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_DELEGUER') or hasRole('ROLE_ENSEIGNANT')" )
    @GetMapping("/list")
    public List<Semestre> list(){
        return service.findsemestre();
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')" )
    @PutMapping("/update")
    public SemestreUpdateDto update(@RequestBody SemestreUpdateDto dto){
        return service.semestreUpdate(dto);
    }

    @GetMapping("/getone/id")
    public Semestre findSemestre(@RequestParam long id){
        return service.getSemestre(id);
    }
}
