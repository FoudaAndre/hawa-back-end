package org.isj.hawa.groupe1.controller;

import org.isj.hawa.groupe1.dto.AnneAcademiqueCreateDto;
import org.isj.hawa.groupe1.dto.AnneeAcademiqueUpdateDto;
import org.isj.hawa.groupe1.serviceInterface.AnneAcademiqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/anneacademique")
public class AnneeAcademiqueController {
    @Autowired
    private AnneAcademiqueService service;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value="/add")
    public AnneeAcademiqueUpdateDto creerAnnee(@RequestBody AnneAcademiqueCreateDto dto){
        return service.anneeCreate(dto);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_DELEGUER') or hasRole('ROLE_ENSEIGNANT')" )
    @GetMapping(value="/list")
    public List<AnneeAcademiqueUpdateDto> listDto(){
        return service.ListeAnnee();
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')" )
    @PutMapping("/update")
    public AnneeAcademiqueUpdateDto updateDto(@RequestBody AnneeAcademiqueUpdateDto annee){
        return service.anneeUpdate(annee);
    }
}
