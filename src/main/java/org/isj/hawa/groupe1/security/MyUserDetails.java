package org.isj.hawa.groupe1.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import org.isj.hawa.groupe1.model.User;
import org.isj.hawa.groupe1.repository.UserRepository;


@Service
public class MyUserDetails implements UserDetailsService {

  @Autowired
  private UserRepository userRepository;

  @Override
  public UserDetails loadUserByUsername(String mail) throws UsernameNotFoundException {
    final User user = userRepository.findByEmail(mail);

    if (user == null) {
        throw new UsernameNotFoundException("no user with email '" + mail + "'found on web site");

    }
      return org.springframework.security.core.userdetails.User//
              .withUsername(user.getEmail())//
              .password(user.getPassword())//
              .authorities(user.getRoles())//
              .accountExpired(false)//
              .accountLocked(false)//
              .credentialsExpired(false)//
              .disabled(false)//
              .build();

  }

}
