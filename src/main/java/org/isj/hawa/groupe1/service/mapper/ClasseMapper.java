package org.isj.hawa.groupe1.service.mapper;


import org.mapstruct.Mapper;
import org.isj.hawa.groupe1.dto.ClasseCreateDto;
import org.isj.hawa.groupe1.dto.ClasseUpdateDto;
import org.isj.hawa.groupe1.model.Classe;

@Mapper
public interface ClasseMapper {

    Classe toEntity(ClasseCreateDto dto);

    ClasseCreateDto toDto(Classe entity);

    Classe toEntityMaj(ClasseUpdateDto dto);

    ClasseUpdateDto toDtoMaj(Classe entity);
}
