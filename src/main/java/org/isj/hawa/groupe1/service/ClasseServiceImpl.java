package org.isj.hawa.groupe1.service;


import org.springframework.stereotype.Service;
import org.isj.hawa.groupe1.dto.ClasseCreateDto;
import org.isj.hawa.groupe1.dto.ClasseUpdateDto;
import org.isj.hawa.groupe1.model.Classe;
import org.isj.hawa.groupe1.repository.ClasseRepository;
import org.isj.hawa.groupe1.service.mapper.ClasseMapper;
import org.isj.hawa.groupe1.serviceInterface.ClasseService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClasseServiceImpl implements ClasseService {

    private final ClasseMapper mapper;
    private final ClasseRepository repository;

    public ClasseServiceImpl(ClasseMapper mapper, ClasseRepository repository) {
        this.mapper = mapper;
        this.repository = repository;
    }

    @Override
    public ClasseUpdateDto creerClasse(ClasseCreateDto dto) {
        final Classe classe = mapper.toEntity(dto);
        return mapper.toDtoMaj(repository.save(classe));
    }

    @Override
    public List<ClasseUpdateDto> ListesClass() {
        return repository.findAll().stream().map(mapper::toDtoMaj).collect(Collectors.toList());
    }

    @Override
    public ClasseUpdateDto updateClass(ClasseUpdateDto dto) {
        final Classe classe = repository.getOne(dto.getId());
        classe.setNiveau(dto.getNiveau());
        classe.setFilier(dto.getFilier());
        classe.setSpecialite(dto.getSpecialite());
        return mapper.toDtoMaj(repository.save(classe));
    }

    @Override
    public Classe findbyId(Long id) {
        return repository.getOne(id);
    }
}
