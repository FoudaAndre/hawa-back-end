package org.isj.hawa.groupe1.service.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import org.isj.hawa.groupe1.dto.AnneAcademiqueCreateDto;
import org.isj.hawa.groupe1.dto.AnneeAcademiqueUpdateDto;
import org.isj.hawa.groupe1.model.AnneeAcademique;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        componentModel = "spring")
public interface AnneeAcademiqueMapper {
    /**
     * To entity.
     *
     * @param dto the dto
     * @return the activity sector
     */

    AnneeAcademique toEntity(AnneAcademiqueCreateDto dto);

    /**
     * To dto.
     *
     * @param entity the entity
     * @return the activity sector dto
     */
    AnneAcademiqueCreateDto toDto(AnneeAcademique entity);

    /**
     * To dto.
     *
     * @param entity the entity
     * @return the activity sector dto
     */
    AnneeAcademiqueUpdateDto toDtoMaj(AnneeAcademique entity);

    /**
     * To dto.
     *
     * @param dto the entity
     * @return the activity sector dto
     */
    AnneeAcademique toEntityMaj(AnneeAcademiqueUpdateDto dto);

}
