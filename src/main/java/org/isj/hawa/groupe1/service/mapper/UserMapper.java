package org.isj.hawa.groupe1.service.mapper;



import org.isj.hawa.groupe1.dto.UserUpdateDto;
import org.isj.hawa.groupe1.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        componentModel = "spring")
public interface UserMapper {

    UserUpdateDto toDtoMaj(User entity);
}
