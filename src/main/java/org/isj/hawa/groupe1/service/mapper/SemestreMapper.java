package org.isj.hawa.groupe1.service.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import org.isj.hawa.groupe1.dto.SemestreCreateDto;
import org.isj.hawa.groupe1.dto.SemestreUpdateDto;
import org.isj.hawa.groupe1.model.Semestre;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        componentModel = "spring")
public interface SemestreMapper {

    Semestre toEntity(SemestreCreateDto dto);

    SemestreCreateDto toDto(Semestre entity);

    Semestre toEntityMaj(SemestreUpdateDto dto);

    SemestreUpdateDto toDtoMaj(Semestre entity);
}
