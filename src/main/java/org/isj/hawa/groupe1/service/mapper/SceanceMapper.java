package org.isj.hawa.groupe1.service.mapper;


import org.isj.hawa.groupe1.dto.SceanceCreateDto;
import org.mapstruct.Mapper;
import org.isj.hawa.groupe1.dto.SceanceUpdateDto;
import org.isj.hawa.groupe1.model.Sceance;

@Mapper
public interface SceanceMapper {

    Sceance toEntity(SceanceCreateDto dto);

    SceanceCreateDto toDto(Sceance entity);

    SceanceUpdateDto toDtoMaj(Sceance entity);

    Sceance toEntityMaj(SceanceUpdateDto dto);

}
