package org.isj.hawa.groupe1.service;


import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.isj.hawa.groupe1.dto.SemestreCreateDto;
import org.isj.hawa.groupe1.dto.SemestreUpdateDto;
import org.isj.hawa.groupe1.model.Semestre;
import org.isj.hawa.groupe1.repository.AnneeAcademiqueRepository;
import org.isj.hawa.groupe1.repository.SemestreRepository;
import org.isj.hawa.groupe1.service.mapper.SemestreMapper;
import org.isj.hawa.groupe1.serviceInterface.SemestreService;

import java.util.Date;
import java.util.List;

@Service
public class SemestreServiceImpl implements SemestreService {

    private final SemestreMapper mapper;
    private final SemestreRepository repository;
    private final AnneeAcademiqueRepository anneeAcademiqueRepository;

    public SemestreServiceImpl(SemestreMapper mapper, SemestreRepository repository, AnneeAcademiqueRepository anneeAcademiqueRepository) {
        this.mapper = mapper;
        this.repository = repository;
        this.anneeAcademiqueRepository = anneeAcademiqueRepository;
    }

    @Override
    public SemestreUpdateDto semestrecreate(SemestreCreateDto dto) {
        final Semestre semestre = mapper.toEntity(dto);
        final Semestre semestreP = repository.findByIsCurrent(true);
        if (semestreP == null) {
            semestre.setCurrent(true);
        } else {
            semestre.setCurrent(false);
        }
        semestre.setAnnee_academique(anneeAcademiqueRepository.getOne(dto.getIdAnnee()));
        return mapper.toDtoMaj(repository.save(semestre));
    }

    @Override
    public SemestreUpdateDto semestreUpdate(SemestreUpdateDto dto) {
        final Semestre semestre = repository.getOne(dto.getId());
        semestre.setNomSemestre(dto.getNomSemestre());
        semestre.setDateDebut(dto.getDateDebut());
        semestre.setDateFin(dto.getDateFin());
        return mapper.toDtoMaj(repository.save(semestre));
    }

    @Override
    public List<Semestre> findsemestre() {
        return repository.findAll(Sort.by(Sort.Direction.DESC,"semestre.annee_academique.nom"));
    }

    @Override
    public Semestre getSemestre(Long id) {
        return repository.getOne(id);
    }
}
