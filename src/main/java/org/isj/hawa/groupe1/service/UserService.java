package org.isj.hawa.groupe1.service;


import org.isj.hawa.groupe1.dto.DeleguerDataDTO;
import org.isj.hawa.groupe1.dto.UserUpdateDto;
import org.isj.hawa.groupe1.repository.ClasseRepository;
import org.isj.hawa.groupe1.service.mapper.UserMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import org.isj.hawa.groupe1.dto.ChangePasswordDTO;
import org.isj.hawa.groupe1.exception.CustomException;
import org.isj.hawa.groupe1.model.Deleguer;
import org.isj.hawa.groupe1.model.Role;
import org.isj.hawa.groupe1.model.User;
import org.isj.hawa.groupe1.repository.UserRepository;
import org.isj.hawa.groupe1.security.JwtTokenProvider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private ModelMapper modelMapper;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Autowired
  private JwtTokenProvider jwtTokenProvider;

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private ClasseRepository classeRepository;

  @Autowired
  private UserMapper mapper;


  public String signin(String email, String password) {
      try {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        return jwtTokenProvider.createToken(email, userRepository.findByEmail(email).getRoles());
      } catch (AuthenticationException e) {
        throw new CustomException("Invalid username/password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
      }
  }


  public String signupAdmin(User user) {
    if (!userRepository.existsByEmail(user.getEmail())) {
      user.setPassword(passwordEncoder.encode(user.getPassword()));
      user.setRoles(new ArrayList<Role>(Arrays.asList(Role.ROLE_ADMIN)));
      userRepository.save(user);
      return jwtTokenProvider.createToken(user.getEmail(), user.getRoles());
    } else {
      throw new CustomException("Username is already in use", HttpStatus.UNPROCESSABLE_ENTITY);
    }
  }

  public String signUpEnseignant(User user) {
    if (!userRepository.existsByEmail(user.getEmail())) {
      user.setPassword(passwordEncoder.encode(user.getPassword()));
      user.setRoles(new ArrayList<Role>(Arrays.asList(Role.ROLE_ENSEIGNANT)));
      userRepository.save(user);
      return jwtTokenProvider.createToken(user.getEmail(), user.getRoles());
    } else {
      throw new CustomException("Username is already in use", HttpStatus.UNPROCESSABLE_ENTITY);
    }
  }

  public void signUpDeleguer(DeleguerDataDTO user) {
    Deleguer deleguer = new Deleguer();
    if (!userRepository.existsByEmail(user.getEmail())) {
      deleguer.setPassword(passwordEncoder.encode(user.getPassword()));
      deleguer.setEmail(user.getEmail());
      deleguer.setMatricule(user.getMatricule());
      deleguer.setRoles(new ArrayList<Role>(Arrays.asList(Role.ROLE_DELEGUER)));
      deleguer.setNom(user.getNom());
      deleguer.setPrenom(user.getPrenom());
      deleguer.setTelephone(user.getTelephone());
      deleguer.setClasse(classeRepository.getOne(user.getIdClasse()));
      userRepository.save(deleguer);
    } else {
      throw new CustomException("Username is already in use", HttpStatus.UNPROCESSABLE_ENTITY);
    }
  }

  public void delete(String username) {

  }

  public User searchUser(String username) {
    User user = userRepository.findByEmail(username);
    if (user == null) {
      throw new CustomException("The user doesn't exist", HttpStatus.NOT_FOUND);
    }
    return user;
  }

  public User whoami() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    String name = authentication.getName();
    return userRepository.findByEmail(name);
  }

  public String refresh(String email) {
    return jwtTokenProvider.createToken(email, userRepository.findByEmail(email).getRoles());
  }

  public String passwordChange(ChangePasswordDTO passwordDTO){
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    String name = authentication.getName();
    User user = userRepository.findByEmail(name);
    if(passwordEncoder.matches(passwordDTO.getAncienPassword(), user.getPassword() )){
      user.setPassword(passwordEncoder.encode(passwordDTO.getPassword()));
      userRepository.save(user);
      return "well done";
    }
   throw new CustomException("Invalid password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
  }

  public void updateUserInfo(UserUpdateDto user){
    User userU = userRepository.findByEmail(user.getEmail());
    userU.setNom(user.getNom());
    userU.setPrenom(user.getPrenom());
    userU.setEmail(user.getEmail());
    userU.setTelephone(user.getTelephone());
    userU.setGrade(user.getGrade());
    userRepository.save(userU);
  }

  public void updateDeleguerInfo(User user){
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    String name = authentication.getName();
    User userU = userRepository.findByEmail(name);
    userU.setNom(user.getNom());
    userU.setPrenom(user.getPrenom());
    userU.setEmail(user.getEmail());
    userU.setTelephone(user.getTelephone());
    userRepository.save(userU);
  }

  public List<UserUpdateDto> listUtilisateur(){
    return userRepository.findAll().stream().map(mapper::toDtoMaj).collect(Collectors.toList());
  }

}
