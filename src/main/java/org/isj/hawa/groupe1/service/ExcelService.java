package org.isj.hawa.groupe1.service;


import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.isj.hawa.groupe1.dto.ResultRequete;
import org.isj.hawa.groupe1.model.*;
import org.isj.hawa.groupe1.repository.ClasseRepository;
import org.isj.hawa.groupe1.repository.SceanceRepository;
import org.isj.hawa.groupe1.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ExcelService {

    private XSSFWorkbook workbook = new XSSFWorkbook();
    private XSSFSheet sheet;
    private List<User> listUsers;
    private List<Classe> classes;
    private List<Sceance> sceances;
    List<ResultExcel> excels;
    @Autowired
    private UserRepository repository;

    @Autowired
    private SceanceRepository sceanceRepository;

    @Autowired
    private ClasseRepository classeRepository;

    String nom = new String();

    int calcul = 0;

    public ExcelService(List<User> listUsers, List<Classe> classes, List<Sceance> sceances, List<ResultExcel> excels) {
        this.listUsers = listUsers;
        this.classes = classes;
        this.sceances = sceances;
        this.excels = excels;
    }

    private void writeHeaderLine() {
        sheet = workbook.createSheet("Users");

        Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(false);
        font.setFontHeight(12);
        style.setFont(font);
        style.setWrapText(true);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        Row row1 = sheet.createRow(1);
        int i = 3;
        int k = 3;
        int ko = 0;
        int index0=1;
        int index1=2;
        int index2=3;
        int index3=3;

        int index4=0;
        int index5=0;
        int index6=3;
        int index7=3;



        sheet.addMergedRegion(CellRangeAddress.valueOf("A1:A3"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("B1:B3"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("C1:C3"));

        createCell(row, 0, "Enseignant", style);
        createCell(row, 1, "Grade", style);
        createCell(row, 2, "Adresse Mail", style);

        classes = classeRepository.findAll(Sort.by(Sort.Direction.ASC,"specialite"));

        sceances = sceanceRepository.findAll(Sort.by(Sort.Direction.ASC,"dateSceance"));
        List<LocalDate> differentDate = new ArrayList<>();
        for (Sceance sceance : sceances) {
            differentDate.add(sceance.getDateSceance());
        }

        List<LocalDate> dateReel = differentDate.stream().distinct().collect(Collectors.toList());

        System.out.println(dateReel);

        LocalDate dateDebut = dateReel.get(0);
        LocalDate dateFin = dateReel.get(dateReel.size()-1);


        List<LocalDate> dater = new ArrayList<>();
        System.out.println(dateDebut + " " + dateFin);

        Long durer = ChronoUnit.DAYS.between(dateDebut, dateFin);

        System.out.println(durer);

        int longuer = Integer.parseInt(String.valueOf(durer));

        longuer = longuer / 7;

        System.out.println(longuer);

        List<LocalDate> datefinal= new ArrayList<>();

        for(int j = 0 ; j<= longuer; j++){
            datefinal.add(dateDebut);
            dateDebut = dateDebut.plusDays(7);
        }

        List<ResultRequete> resultExcels = sceanceRepository.findByAll();

        for (ResultRequete object :
                resultExcels) {
            ResultExcel excel = new ResultExcel();
            excel.setClasse(object.getClasse());
            excel.setNom(object.getNom());
            excel.setEmail(object.getEmail());
            excel.setGrade(object.getGrade());
            excel.setDate_sceance(object.getDate_sceance());
            if(object.getTotalheur() == null){
                excel.setTotalheur(00);
            }else{
                excel.setTotalheur(Float.valueOf(object.getTotalheur()));
            }
            if(object.getSemaine() == null){
                excel.setSemaine(00);
            }else{
                excel.setSemaine(Integer.valueOf(object.getSemaine()));
            }
            excels.add(excel);
            System.out.println(excel);
        }


        System.out.println(datefinal);

        for (LocalDate date: datefinal) {
            calcul++;
            if (ko == 0) {
                index7 = index6 + classes.size();
                ko++;
            }
            createCell(row, k++, "from:" + date.plusDays(1).toString() + " To:" + date.plusDays(7).toString(), style);
            sheet.addMergedRegion(new CellRangeAddress(index4,index5,index6, index7));
            index6= index6 + classes.size()+1;
            index7= index7 + classes.size()+1;
            k = k + classes.size();

            for (Classe classe : classes) {
                createCell(row1, i++, classe.getFilier() + classe.getNiveau() + classe.getSpecialite(), style);
                sheet.addMergedRegion(new CellRangeAddress(index0, index1, index2, index3));
                index2++;
                index3++;
            }

            createCell(row1, i++,"cours"  , style);
            sheet.addMergedRegion(new CellRangeAddress(index0, index1, index2, index3));
            index2++;
            index3++;

        }

    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }else if(value instanceof Float) {
            cell.setCellValue((Float) value);
        }else{
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void writeDataLines() {
        int rowCount = 2;
        int rowcount = 0;
        Row row = sheet.createRow(rowCount++);

        this.listUsers = repository.findAllByRoles(Role.ROLE_ENSEIGNANT);
        System.out.println(this.listUsers.toString());

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(12);
        style.setFont(font);

        int columnCount = 0;

        for (ResultExcel exceler : excels) {

            if(this.nom.equals(exceler.getEmail())){
                System.out.println(this.sceances.toString());

                for (Classe classes : classes) {
                    if (exceler.getClasse() == null){
                        createCell(row, columnCount++, exceler.getTotalheur(), style);
                    }else {
                        String classer = classes.getFilier()+classes.getNiveau()+classes.getSpecialite();
                        System.out.println(classer);
                        if(exceler.getClasse().equals(classer)){
                            createCell(row, columnCount++, exceler.getTotalheur(), style);
                        }else{
                            createCell(row, columnCount++, 0, style);
                        }
                    }
                }

                columnCount++;

                this.nom= exceler.getEmail();
            }
            if(!this.nom.equals(exceler.getEmail())){

                row = sheet.createRow(rowCount++);
                columnCount = 0;

                createCell(row, columnCount++, exceler.getNom(), style);
                createCell(row, columnCount++, exceler.getGrade(), style);
                createCell(row, columnCount++, exceler.getEmail(), style);
                for (Classe classe : classes) {

                    if (exceler.getClasse() == null){
                        createCell(row, columnCount++, exceler.getTotalheur(), style);
                    }else {
                        String classer = classe.getFilier()+classe.getNiveau()+classe.getSpecialite();
                        System.out.println(classer);
                        if(exceler.getClasse().equals(classer)){
                            createCell(row, columnCount++, exceler.getTotalheur(), style);
                        }else{
                            createCell(row, columnCount++, 0, style);
                        }
                    }

                }
                columnCount++;

                if(exceler.getClasse() == null){
                    for(int i=0; i< calcul-1; i++){
                        for (Classe classers: classes) {
                            createCell(row, columnCount++, 0, style);
                        }
                        columnCount++;
                    }

                }
                this.nom = exceler.getEmail();

            }
        }

    }

    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();

    }
}

