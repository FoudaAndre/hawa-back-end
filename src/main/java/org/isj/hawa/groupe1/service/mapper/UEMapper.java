package org.isj.hawa.groupe1.service.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import org.isj.hawa.groupe1.dto.UECreateDto;
import org.isj.hawa.groupe1.dto.UEUpdateDto;
import org.isj.hawa.groupe1.model.UE;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        componentModel = "spring")
public interface UEMapper {

    UE toEntity(UECreateDto dto);

    UECreateDto toDto(UE entity);

    UE toEntityMaj(UEUpdateDto dto);

    UEUpdateDto toDtoMaj(UE entity);
}
