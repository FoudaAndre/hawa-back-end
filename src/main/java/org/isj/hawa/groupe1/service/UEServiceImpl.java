package org.isj.hawa.groupe1.service;


import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.isj.hawa.groupe1.dto.UECreateDto;
import org.isj.hawa.groupe1.dto.UEUpdateDto;
import org.isj.hawa.groupe1.model.UE;
import org.isj.hawa.groupe1.repository.UeRepository;
import org.isj.hawa.groupe1.service.mapper.UEMapper;
import org.isj.hawa.groupe1.serviceInterface.UEService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class UEServiceImpl implements UEService {

    private final UEMapper mapper;
    private final UeRepository repository;

    public UEServiceImpl(UEMapper mapper, UeRepository repository) {
        this.mapper = mapper;
        this.repository = repository;
    }


    @Override
    public UEUpdateDto creerUE(UECreateDto dto) {
        final UE uE =mapper.toEntity(dto);
        return mapper.toDtoMaj(repository.save(uE));
    }

    @Override
    public UEUpdateDto updateUE(UEUpdateDto dto) {
        final UE uE = repository.getOne(dto.getId());
        uE.setNom(dto.getNom());
        return mapper.toDtoMaj(repository.save(uE));
    }

    @Override
    public List<UEUpdateDto> listUE() {
        return repository.findAll(Sort.by(Sort.Direction.ASC,"nom"))
                .stream().map(mapper::toDtoMaj).collect(Collectors.toList());
    }
}
