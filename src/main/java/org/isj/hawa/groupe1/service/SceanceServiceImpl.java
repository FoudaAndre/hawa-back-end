package org.isj.hawa.groupe1.service;


import org.isj.hawa.groupe1.dto.SceanceCreateDto;
import org.isj.hawa.groupe1.dto.SceanceUpdateDto;
import org.isj.hawa.groupe1.model.Sceance;
import org.isj.hawa.groupe1.repository.*;
import org.isj.hawa.groupe1.service.mapper.SceanceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.isj.hawa.groupe1.serviceInterface.SceanceService;

import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SceanceServiceImpl implements SceanceService {

    private SceanceMapper mapper;
    private SceanceRepository repository;
    private SemestreRepository semestreRepository;
    private UserRepository userRepository;
    private ClasseRepository classeRepository;
    private UeRepository ueRepository;

    @Autowired
    public SceanceServiceImpl(SceanceMapper mapper, SceanceRepository repository, SemestreRepository semestreRepository, UserRepository userRepository, ClasseRepository classeRepository, UeRepository ueRepository) {
        this.mapper = mapper;
        this.repository = repository;
        this.semestreRepository = semestreRepository;
        this.userRepository = userRepository;
        this.classeRepository = classeRepository;
        this.ueRepository = ueRepository;
    }

    @Override
    public SceanceUpdateDto CreerSceance(SceanceCreateDto dto) {
        Sceance sceance = new Sceance();
        sceance.setHeurDebutPrevu(dto.getHeurDebutPrevu());
        sceance.setHeurFinPrevu(dto.getHeurFinPrevu());
        sceance.setDateSceance(dto.getDateSceance());
        sceance.setCours(ueRepository.getOne(dto.getIdUE()));
        sceance.setEnseignant(userRepository.findByEmail(dto.getMailEnseignant()));
        sceance.setClasses(classeRepository.getOne(dto.getIdClasses()));
        sceance.setSemestre(semestreRepository.findByIsCurrent(true));
        return mapper.toDtoMaj(repository.save(sceance));
    }

    @Override
    public SceanceUpdateDto updateSceance(SceanceUpdateDto dto) {
        Sceance sceance = repository.getOne(dto.getId());
        sceance.setHeurDebutPrevu(dto.getHeurDebutPrevu());
        sceance.setHeurFinPrevu(dto.getHeurFinPrevu());
        sceance.setDateSceance(dto.getDateSceance());
        sceance.setCours(ueRepository.getOne(dto.getIdUE()));
        sceance.setEnseignant(userRepository.findByEmail(dto.getMailEnseignant()));
        sceance.setClasses(classeRepository.getOne(dto.getIdClasses()));
        sceance.setSemestre(semestreRepository.findByIsCurrent(true));
        return mapper.toDtoMaj(repository.save(sceance));
    }

    @Override
    public List<SceanceUpdateDto> listSceance() {
        return repository.findAll().stream()
                .map(mapper::toDtoMaj).collect(Collectors.toList());
    }

    @Override
    public void startClasse(Long id) {
        Sceance sceance = repository.getOne(id);
        sceance.setHeurDebutReel(LocalTime.now());
        repository.save(sceance);
    }

    @Override
    public void endClasse(Long id) {
        Sceance sceance = repository.getOne(id);
        sceance.setHeurFinReel(LocalTime.now());
        repository.save(sceance);
    }

}
