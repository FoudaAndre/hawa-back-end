package org.isj.hawa.groupe1.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.isj.hawa.groupe1.dto.AnneAcademiqueCreateDto;
import org.isj.hawa.groupe1.dto.AnneeAcademiqueUpdateDto;
import org.isj.hawa.groupe1.model.AnneeAcademique;
import org.isj.hawa.groupe1.repository.AnneeAcademiqueRepository;
import org.isj.hawa.groupe1.service.mapper.AnneeAcademiqueMapper;
import org.isj.hawa.groupe1.serviceInterface.AnneAcademiqueService;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class AnneAcademiqueServiceImpl implements AnneAcademiqueService {

    private AnneeAcademiqueRepository repository;
    private AnneeAcademiqueMapper mapper;

    @Autowired
    public AnneAcademiqueServiceImpl(AnneeAcademiqueRepository repository, AnneeAcademiqueMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public AnneeAcademiqueUpdateDto anneeCreate(AnneAcademiqueCreateDto dto) {
        final AnneeAcademique anneepresent = repository.findByIsCurrent(true);
        final AnneeAcademique annee = mapper.toEntity(dto);
        if(anneepresent == null){

            annee.setCurrent(true);
            return mapper.toDtoMaj(repository.save(annee));
        }else{
            annee.setCurrent(false);
            return mapper.toDtoMaj(repository.save(annee));
        }
    }

    @Override
    public AnneeAcademiqueUpdateDto anneeUpdate(AnneeAcademiqueUpdateDto annee) {
        final AnneeAcademique annee1 = repository.getOne(annee.getId());
        annee1.setNomAnnee(annee.getNomAnnee());
        annee1.setDateDebut(annee.getDateDebut());
        annee1.setDateFin(annee.getDateFin());
        return mapper.toDtoMaj(repository.save(annee1));
    }

    @Override
    public List<AnneeAcademiqueUpdateDto> ListeAnnee() {
        return repository.findAll(Sort.by(Sort.Direction.ASC, "nomAnnee")).stream()
                .map(mapper::toDtoMaj).collect(Collectors.toList());
    }


}