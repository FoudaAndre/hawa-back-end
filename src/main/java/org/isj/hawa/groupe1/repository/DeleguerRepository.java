package org.isj.hawa.groupe1.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.isj.hawa.groupe1.model.Deleguer;

@Repository
public interface DeleguerRepository extends JpaRepository<Deleguer, Long> {
}
