package org.isj.hawa.groupe1.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.isj.hawa.groupe1.model.AnneeAcademique;

@Repository
public interface AnneeAcademiqueRepository extends JpaRepository<AnneeAcademique, Long> {
    AnneeAcademique findByIsCurrent(Boolean value);
}
