package org.isj.hawa.groupe1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.isj.hawa.groupe1.model.Semestre;

@Repository
public interface SemestreRepository extends JpaRepository<Semestre, Long> {
    Semestre findByIsCurrent(Boolean value);
}
