package org.isj.hawa.groupe1.repository;

import org.isj.hawa.groupe1.dto.ResultRequete;
import org.isj.hawa.groupe1.model.ResultExcel;
import org.isj.hawa.groupe1.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.isj.hawa.groupe1.model.Sceance;

import java.util.List;

@Repository
public interface SceanceRepository extends JpaRepository<Sceance, Long> {
    @Query(value="select concat(user.nom,\" \",user.prenom) as nom, user.grade, user.email," +
            " answer.classe, answer.date_sceance, SUM(answer.durer) as totalheur," +
            "week(date_sceance) as semaine "+" from ( SELECT concat(nom, prenom) as nom, grade, email, date_sceance," +
            " heur_debut_prevu, heur_fin_prevu," +
            " concat(filier, niveau, specialite) as classe," +
            " time_to_sec(timediff(heur_fin_prevu, heur_debut_prevu))/3600 as durer FROM classe, user, sceance" +
            " where sceance.enseignant=user.id and sceance.classes = classe.id)answer RIGHT outer JOIN" +
            " user on answer.email = user.email INNER JOIN user_roles ON user.id = user_roles.user_id " +
            "WHERE user_roles.roles = 'ROLE_ENSEIGNANT' GROUP by user.email, semaine", nativeQuery = true)
    List<ResultRequete> findByAll();

    List<Sceance> findAllByEnseignant(User user);
}
