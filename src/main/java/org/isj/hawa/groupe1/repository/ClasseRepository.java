package org.isj.hawa.groupe1.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.isj.hawa.groupe1.model.Classe;

@Repository
public interface ClasseRepository extends JpaRepository<Classe, Long> {
}
