package org.isj.hawa.groupe1.repository;

import org.isj.hawa.groupe1.model.Role;
import org.isj.hawa.groupe1.model.Statut;
import org.springframework.data.jpa.repository.JpaRepository;
import org.isj.hawa.groupe1.model.User;

import java.util.List;


public interface UserRepository extends JpaRepository<User, Long> {

  boolean existsByEmail(String email);

  User findByEmail(String username);

  List<User> findAllByRoles(Role role);

}
