package org.isj.hawa.groupe1.model;

import java.time.LocalDate;

public class ResultExcel {
    private String nom;
    private String grade;
    private String email;
    private String classe;
    private LocalDate date_sceance;
    private float totalheur;
    private int semaine;

    public ResultExcel() {
    }

    public ResultExcel(String nom, String grade, String email, String classe, LocalDate date_sceance, float totalheur, int semaine) {
        this.nom = nom;
        this.grade = grade;
        this.email = email;
        this.classe = classe;
        this.date_sceance = date_sceance;
        this.totalheur = totalheur;
        this.semaine = semaine;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getClasse() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    public LocalDate getDate_sceance() {
        return date_sceance;
    }

    public void setDate_sceance(LocalDate date_sceance) {
        this.date_sceance = date_sceance;
    }

    public float getTotalheur() {
        return totalheur;
    }

    public void setTotalheur(float totalheur) {
        this.totalheur = totalheur;
    }

    public int getSemaine() {
        return semaine;
    }

    public void setSemaine(int semaine) {
        this.semaine = semaine;
    }

    @Override
    public String toString() {
        return "ResultExcel{" +
                "nom='" + nom + '\'' +
                ", grade='" + grade + '\'' +
                ", email='" + email + '\'' +
                ", classe='" + classe + '\'' +
                ", date_sceance=" + date_sceance +
                ", totalheur=" + totalheur +
                ", semaine=" + semaine +
                '}';
    }
}
