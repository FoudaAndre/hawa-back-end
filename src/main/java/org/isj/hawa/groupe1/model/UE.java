package org.isj.hawa.groupe1.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="UE")
public class UE extends Audit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private long Id;

    private String nom;

    @Column(unique = true)
    private String CodeUE;

    @OneToMany
    private List<Sceance> sceances;

    public UE() {
    }

    public UE(long id, String nom, String codeUE) {
        Id = id;
        this.nom = nom;
        CodeUE = codeUE;
    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCodeUE() {
        return CodeUE;
    }

    public void setCodeUE(String codeUE) {
        CodeUE = codeUE;
    }

    public List<Sceance> getSceances() {
        return sceances;
    }

    public void setSceances(List<Sceance> sceances) {
        this.sceances = sceances;
    }


}
