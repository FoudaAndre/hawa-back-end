package org.isj.hawa.groupe1.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="Sceance")
public class Sceance extends Audit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private long id;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateSceance;
    private LocalTime heurDebutPrevu;
    private LocalTime heurFinPrevu;
    private LocalTime heurDebutReel;
    private LocalTime heurFinReel;
    @ManyToOne
    private Classe classes;
    @ManyToOne
    private UE cours;
    @ManyToOne
    private User enseignant;
    @ManyToOne
    private Semestre semestre;

    public Sceance() {
    }

    public Sceance(long id, LocalDate dateSceance, LocalTime heurDebutPrevu, LocalTime heurFinPrevu, LocalTime heurDebutReel, LocalTime heurFinReel) {
        this.id = id;
        this.dateSceance = dateSceance;
        this.heurDebutPrevu = heurDebutPrevu;
        this.heurFinPrevu = heurFinPrevu;
        this.heurDebutReel = heurDebutReel;
        this.heurFinReel = heurFinReel;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getDateSceance() {
        return dateSceance;
    }

    public void setDateSceance(LocalDate dateSceance) {
        this.dateSceance = dateSceance;
    }

    public LocalTime getHeurDebutPrevu() {
        return heurDebutPrevu;
    }

    public void setHeurDebutPrevu(LocalTime heurDebutPrevu) {
        this.heurDebutPrevu = heurDebutPrevu;
    }

    public LocalTime getHeurFinPrevu() {
        return heurFinPrevu;
    }

    public void setHeurFinPrevu(LocalTime heurFinPrevu) {
        this.heurFinPrevu = heurFinPrevu;
    }

    public LocalTime getHeurDebutReel() {
        return heurDebutReel;
    }

    public void setHeurDebutReel(LocalTime heurDebutReel) {
        this.heurDebutReel = heurDebutReel;
    }

    public LocalTime getHeurFinReel() {
        return heurFinReel;
    }

    public void setHeurFinReel(LocalTime heurFinReel) {
        this.heurFinReel = heurFinReel;
    }

    public Classe getClasses() {
        return classes;
    }

    public void setClasses(Classe classes) {
        this.classes = classes;
    }

    public UE getCours() {
        return cours;
    }

    public void setCours(UE cours) {
        this.cours = cours;
    }

    public User getEnseignant() {
        return enseignant;
    }

    public void setEnseignant(User enseignant) {
        this.enseignant = enseignant;
    }

    public Semestre getSemestre() {
        return semestre;
    }

    public void setSemestre(Semestre semestre) {
        this.semestre = semestre;
    }
}
