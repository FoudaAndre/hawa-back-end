package org.isj.hawa.groupe1.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="Classe")
public class Classe extends Audit{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private long Id;
    private String filier;
    private String specialite;
    private int niveau;
    @OneToMany
    private List<Sceance> sceances;

    @OneToOne
    private Deleguer deleguer;

    public Classe() {
    }

    public Classe(long id, String filier, String specialite, int niveau) {
        Id = id;
        this.filier = filier;
        this.specialite = specialite;
        this.niveau = niveau;
    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getFilier() {
        return filier;
    }

    public void setFilier(String filier) {
        this.filier = filier;
    }

    public String getSpecialite() {
        return specialite;
    }

    public void setSpecialite(String specialite) {
        this.specialite = specialite;
    }

    public int getNiveau() {
        return niveau;
    }

    public void setNiveau(int niveau) {
        this.niveau = niveau;
    }

    public List<Sceance> getSceances() {
        return sceances;
    }

    public void setSceances(List<Sceance> sceances) {
        this.sceances = sceances;
    }

    public Deleguer getDeleguer() {
        return deleguer;
    }

    public void setDeleguer(Deleguer deleguer) {
        this.deleguer = deleguer;
    }
}
