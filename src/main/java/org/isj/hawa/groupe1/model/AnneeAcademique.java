package org.isj.hawa.groupe1.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name="Annee_academique")
public class AnneeAcademique extends Audit implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private long Id;
    private String nomAnnee;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateDebut;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateFin;
    private Boolean isCurrent;
    @OneToMany
    private List<Semestre> semestres;

    public AnneeAcademique() {
    }

    public AnneeAcademique(long id, String nomAnnee, LocalDate dateDebut, LocalDate dateFin, Boolean isCurrent) {
        Id = id;
        this.nomAnnee = nomAnnee;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.isCurrent = isCurrent;
    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getNomAnnee() {
        return nomAnnee;
    }

    public void setNomAnnee(String nomAnnee) {
        this.nomAnnee = nomAnnee;
    }

    public LocalDate getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDate getDateFin() {
        return dateFin;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }

    public Boolean getCurrent() {
        return isCurrent;
    }

    public void setCurrent(Boolean current) {
        isCurrent = current;
    }

    public List<Semestre> getSemestres() {
        return semestres;
    }

    public void setSemestres(List<Semestre> semestres) {
        this.semestres = semestres;
    }

}
