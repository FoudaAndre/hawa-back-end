package org.isj.hawa.groupe1.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity
public class User extends Audit implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY )
  private long Id;

  private String nom;

  private String prenom;

  private String grade;

  @Column(unique = true)
  private String email;

  @Column(unique = true)
  private String telephone;

  private String Password;

  @Enumerated(EnumType.STRING)
  @Fetch(value = FetchMode.SUBSELECT)
  @ElementCollection(fetch = FetchType.EAGER)
  List<Role> roles;

  public User() {
  }

  public User(long id, String nom, String prenom, String grade, String email, String telephone, String password, List<Role> roles) {
    Id = id;
    this.nom = nom;
    this.prenom = prenom;
    this.grade = grade;
    this.email = email;
    this.telephone = telephone;
    Password = password;
    this.roles = roles;
  }

  public long getId() {
    return Id;
  }

  public void setId(long id) {
    Id = id;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getPrenom() {
    return prenom;
  }

  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getTelephone() {
    return telephone;
  }

  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }

  public String getPassword() {
    return Password;
  }

  public void setPassword(String password) {
    Password = password;
  }

  public List<Role> getRoles() {
    return roles;
  }

  public void setRoles(List<Role> roles) {
    this.roles = roles;
  }

  public String getGrade() {
    return grade;
  }

  public void setGrade(String grade) {
    this.grade = grade;
  }
}
