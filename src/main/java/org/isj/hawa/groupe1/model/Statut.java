package org.isj.hawa.groupe1.model;

public enum Statut {
    ACTIF, INACTIF;

    Statut() {
    }
}
