package org.isj.hawa.groupe1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="deleguer")
public class Deleguer extends User{

    @Column(unique = true)
    private String matricule;

    public Deleguer(String matricule) {
        this.matricule = matricule;
    }

    public Deleguer() {
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public Classe getClasse() {
        return classe;
    }

    public void setClasse(Classe classe) {
        this.classe = classe;
    }

    @OneToOne
    private Classe classe;
}
