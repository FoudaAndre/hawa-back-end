package org.isj.hawa.groupe1.model;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
  ROLE_ADMIN, ROLE_DELEGUER, ROLE_ENSEIGNANT;

  public String getAuthority() {
    return name();
  }

  //@PreAuthorize("hasRole('ROLE_ADMIN') hasRole('ROLE_DELEGUER') or hasRole('ROLE_ENSEIGNANT')" )
}
