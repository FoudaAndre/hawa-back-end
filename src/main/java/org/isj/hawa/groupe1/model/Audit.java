package org.isj.hawa.groupe1.model;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.*;
import java.util.Calendar;

@MappedSuperclass
public abstract class Audit {

    protected String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    protected Calendar createdDate;

    protected String modifiedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    protected Calendar modifiedDate;

    @PrePersist
    public void prePersist() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        createdDate = Calendar.getInstance();
        createdBy = authentication.getName();
        modifiedDate = Calendar.getInstance();
        modifiedBy = authentication.getName();
    }

    @PreUpdate
    public void preUpdate() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        modifiedDate = Calendar.getInstance();
        modifiedBy = authentication.getName();
    }
}
