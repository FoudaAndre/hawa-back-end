package org.isj.hawa.groupe1.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name="Semestre")
public class Semestre extends Audit implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private long Id;
    private String nomSemestre;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateDebut;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateFin;
    private Boolean isCurrent;
    @OneToMany
    private List<Sceance> sceances;
    @ManyToOne
    private AnneeAcademique annee_academique;

    public Semestre() {
    }

    public Semestre(long id, String nomSemestre, LocalDate dateDebut, LocalDate dateFin, Boolean isCurrent) {
        Id = id;
        this.nomSemestre = nomSemestre;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.isCurrent = isCurrent;
    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getNomSemestre() {
        return nomSemestre;
    }

    public void setNomSemestre(String nomSemestre) {
        this.nomSemestre = nomSemestre;
    }

    public LocalDate getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDate getDateFin() {
        return dateFin;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }

    public Boolean getCurrent() {
        return isCurrent;
    }

    public void setCurrent(Boolean current) {
        isCurrent = current;
    }

    public List<Sceance> getSceances() {
        return sceances;
    }

    public void setSceances(List<Sceance> sceances) {
        this.sceances = sceances;
    }

    public AnneeAcademique getAnnee_academique() {
        return annee_academique;
    }

    public void setAnnee_academique(AnneeAcademique annee_academique) {
        this.annee_academique = annee_academique;
    }
}
