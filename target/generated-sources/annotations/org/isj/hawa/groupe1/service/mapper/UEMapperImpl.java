package org.isj.hawa.groupe1.service.mapper;

import javax.annotation.Generated;
import org.isj.hawa.groupe1.dto.UECreateDto;
import org.isj.hawa.groupe1.dto.UEUpdateDto;
import org.isj.hawa.groupe1.model.UE;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-05-10T12:04:35+0100",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.14.1 (SAP SE)"
)
@Component
public class UEMapperImpl implements UEMapper {

    @Override
    public UE toEntity(UECreateDto dto) {
        if ( dto == null ) {
            return null;
        }

        UE uE = new UE();

        if ( dto.getNom() != null ) {
            uE.setNom( dto.getNom() );
        }
        if ( dto.getCodeUE() != null ) {
            uE.setCodeUE( dto.getCodeUE() );
        }

        return uE;
    }

    @Override
    public UECreateDto toDto(UE entity) {
        if ( entity == null ) {
            return null;
        }

        UECreateDto uECreateDto = new UECreateDto();

        if ( entity.getNom() != null ) {
            uECreateDto.setNom( entity.getNom() );
        }
        if ( entity.getCodeUE() != null ) {
            uECreateDto.setCodeUE( entity.getCodeUE() );
        }

        return uECreateDto;
    }

    @Override
    public UE toEntityMaj(UEUpdateDto dto) {
        if ( dto == null ) {
            return null;
        }

        UE uE = new UE();

        uE.setId( dto.getId() );
        if ( dto.getNom() != null ) {
            uE.setNom( dto.getNom() );
        }
        if ( dto.getCodeUE() != null ) {
            uE.setCodeUE( dto.getCodeUE() );
        }

        return uE;
    }

    @Override
    public UEUpdateDto toDtoMaj(UE entity) {
        if ( entity == null ) {
            return null;
        }

        UEUpdateDto uEUpdateDto = new UEUpdateDto();

        uEUpdateDto.setId( entity.getId() );
        if ( entity.getNom() != null ) {
            uEUpdateDto.setNom( entity.getNom() );
        }
        if ( entity.getCodeUE() != null ) {
            uEUpdateDto.setCodeUE( entity.getCodeUE() );
        }

        return uEUpdateDto;
    }
}
