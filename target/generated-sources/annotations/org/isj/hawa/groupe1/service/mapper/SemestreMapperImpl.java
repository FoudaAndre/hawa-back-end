package org.isj.hawa.groupe1.service.mapper;

import javax.annotation.Generated;
import org.isj.hawa.groupe1.dto.SemestreCreateDto;
import org.isj.hawa.groupe1.dto.SemestreUpdateDto;
import org.isj.hawa.groupe1.model.Semestre;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-05-10T12:04:35+0100",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.14.1 (SAP SE)"
)
@Component
public class SemestreMapperImpl implements SemestreMapper {

    @Override
    public Semestre toEntity(SemestreCreateDto dto) {
        if ( dto == null ) {
            return null;
        }

        Semestre semestre = new Semestre();

        if ( dto.getNomSemestre() != null ) {
            semestre.setNomSemestre( dto.getNomSemestre() );
        }
        if ( dto.getDateDebut() != null ) {
            semestre.setDateDebut( dto.getDateDebut() );
        }
        if ( dto.getDateFin() != null ) {
            semestre.setDateFin( dto.getDateFin() );
        }

        return semestre;
    }

    @Override
    public SemestreCreateDto toDto(Semestre entity) {
        if ( entity == null ) {
            return null;
        }

        SemestreCreateDto semestreCreateDto = new SemestreCreateDto();

        if ( entity.getNomSemestre() != null ) {
            semestreCreateDto.setNomSemestre( entity.getNomSemestre() );
        }
        if ( entity.getDateDebut() != null ) {
            semestreCreateDto.setDateDebut( entity.getDateDebut() );
        }
        if ( entity.getDateFin() != null ) {
            semestreCreateDto.setDateFin( entity.getDateFin() );
        }

        return semestreCreateDto;
    }

    @Override
    public Semestre toEntityMaj(SemestreUpdateDto dto) {
        if ( dto == null ) {
            return null;
        }

        Semestre semestre = new Semestre();

        semestre.setId( dto.getId() );
        if ( dto.getNomSemestre() != null ) {
            semestre.setNomSemestre( dto.getNomSemestre() );
        }
        if ( dto.getDateDebut() != null ) {
            semestre.setDateDebut( dto.getDateDebut() );
        }
        if ( dto.getDateFin() != null ) {
            semestre.setDateFin( dto.getDateFin() );
        }

        return semestre;
    }

    @Override
    public SemestreUpdateDto toDtoMaj(Semestre entity) {
        if ( entity == null ) {
            return null;
        }

        SemestreUpdateDto semestreUpdateDto = new SemestreUpdateDto();

        semestreUpdateDto.setId( entity.getId() );
        if ( entity.getNomSemestre() != null ) {
            semestreUpdateDto.setNomSemestre( entity.getNomSemestre() );
        }
        if ( entity.getDateDebut() != null ) {
            semestreUpdateDto.setDateDebut( entity.getDateDebut() );
        }
        if ( entity.getDateFin() != null ) {
            semestreUpdateDto.setDateFin( entity.getDateFin() );
        }

        return semestreUpdateDto;
    }
}
