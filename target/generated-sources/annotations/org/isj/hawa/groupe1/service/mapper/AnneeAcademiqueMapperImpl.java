package org.isj.hawa.groupe1.service.mapper;

import javax.annotation.Generated;
import org.isj.hawa.groupe1.dto.AnneAcademiqueCreateDto;
import org.isj.hawa.groupe1.dto.AnneeAcademiqueUpdateDto;
import org.isj.hawa.groupe1.model.AnneeAcademique;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-05-10T12:04:36+0100",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.14.1 (SAP SE)"
)
@Component
public class AnneeAcademiqueMapperImpl implements AnneeAcademiqueMapper {

    @Override
    public AnneeAcademique toEntity(AnneAcademiqueCreateDto dto) {
        if ( dto == null ) {
            return null;
        }

        AnneeAcademique anneeAcademique = new AnneeAcademique();

        if ( dto.getNomAnnee() != null ) {
            anneeAcademique.setNomAnnee( dto.getNomAnnee() );
        }
        if ( dto.getDateDebut() != null ) {
            anneeAcademique.setDateDebut( dto.getDateDebut() );
        }
        if ( dto.getDateFin() != null ) {
            anneeAcademique.setDateFin( dto.getDateFin() );
        }

        return anneeAcademique;
    }

    @Override
    public AnneAcademiqueCreateDto toDto(AnneeAcademique entity) {
        if ( entity == null ) {
            return null;
        }

        AnneAcademiqueCreateDto anneAcademiqueCreateDto = new AnneAcademiqueCreateDto();

        if ( entity.getNomAnnee() != null ) {
            anneAcademiqueCreateDto.setNomAnnee( entity.getNomAnnee() );
        }
        if ( entity.getDateDebut() != null ) {
            anneAcademiqueCreateDto.setDateDebut( entity.getDateDebut() );
        }
        if ( entity.getDateFin() != null ) {
            anneAcademiqueCreateDto.setDateFin( entity.getDateFin() );
        }

        return anneAcademiqueCreateDto;
    }

    @Override
    public AnneeAcademiqueUpdateDto toDtoMaj(AnneeAcademique entity) {
        if ( entity == null ) {
            return null;
        }

        AnneeAcademiqueUpdateDto anneeAcademiqueUpdateDto = new AnneeAcademiqueUpdateDto();

        anneeAcademiqueUpdateDto.setId( entity.getId() );
        if ( entity.getNomAnnee() != null ) {
            anneeAcademiqueUpdateDto.setNomAnnee( entity.getNomAnnee() );
        }
        if ( entity.getDateDebut() != null ) {
            anneeAcademiqueUpdateDto.setDateDebut( entity.getDateDebut() );
        }
        if ( entity.getDateFin() != null ) {
            anneeAcademiqueUpdateDto.setDateFin( entity.getDateFin() );
        }

        return anneeAcademiqueUpdateDto;
    }

    @Override
    public AnneeAcademique toEntityMaj(AnneeAcademiqueUpdateDto dto) {
        if ( dto == null ) {
            return null;
        }

        AnneeAcademique anneeAcademique = new AnneeAcademique();

        anneeAcademique.setId( dto.getId() );
        if ( dto.getNomAnnee() != null ) {
            anneeAcademique.setNomAnnee( dto.getNomAnnee() );
        }
        if ( dto.getDateDebut() != null ) {
            anneeAcademique.setDateDebut( dto.getDateDebut() );
        }
        if ( dto.getDateFin() != null ) {
            anneeAcademique.setDateFin( dto.getDateFin() );
        }

        return anneeAcademique;
    }
}
