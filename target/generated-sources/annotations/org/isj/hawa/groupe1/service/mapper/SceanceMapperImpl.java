package org.isj.hawa.groupe1.service.mapper;

import javax.annotation.Generated;
import org.isj.hawa.groupe1.dto.SceanceCreateDto;
import org.isj.hawa.groupe1.dto.SceanceUpdateDto;
import org.isj.hawa.groupe1.model.Sceance;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-05-10T12:04:35+0100",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.14.1 (SAP SE)"
)
@Component
public class SceanceMapperImpl implements SceanceMapper {

    @Override
    public Sceance toEntity(SceanceCreateDto dto) {
        if ( dto == null ) {
            return null;
        }

        Sceance sceance = new Sceance();

        sceance.setDateSceance( dto.getDateSceance() );
        sceance.setHeurDebutPrevu( dto.getHeurDebutPrevu() );
        sceance.setHeurFinPrevu( dto.getHeurFinPrevu() );

        return sceance;
    }

    @Override
    public SceanceCreateDto toDto(Sceance entity) {
        if ( entity == null ) {
            return null;
        }

        SceanceCreateDto sceanceCreateDto = new SceanceCreateDto();

        sceanceCreateDto.setDateSceance( entity.getDateSceance() );
        sceanceCreateDto.setHeurDebutPrevu( entity.getHeurDebutPrevu() );
        sceanceCreateDto.setHeurFinPrevu( entity.getHeurFinPrevu() );

        return sceanceCreateDto;
    }

    @Override
    public SceanceUpdateDto toDtoMaj(Sceance entity) {
        if ( entity == null ) {
            return null;
        }

        SceanceUpdateDto sceanceUpdateDto = new SceanceUpdateDto();

        sceanceUpdateDto.setId( entity.getId() );
        sceanceUpdateDto.setDateSceance( entity.getDateSceance() );
        sceanceUpdateDto.setHeurDebutPrevu( entity.getHeurDebutPrevu() );
        sceanceUpdateDto.setHeurFinPrevu( entity.getHeurFinPrevu() );

        return sceanceUpdateDto;
    }

    @Override
    public Sceance toEntityMaj(SceanceUpdateDto dto) {
        if ( dto == null ) {
            return null;
        }

        Sceance sceance = new Sceance();

        if ( dto.getId() != null ) {
            sceance.setId( dto.getId() );
        }
        sceance.setDateSceance( dto.getDateSceance() );
        sceance.setHeurDebutPrevu( dto.getHeurDebutPrevu() );
        sceance.setHeurFinPrevu( dto.getHeurFinPrevu() );

        return sceance;
    }
}
