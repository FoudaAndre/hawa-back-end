package org.isj.hawa.groupe1.service.mapper;

import javax.annotation.Generated;
import org.isj.hawa.groupe1.dto.ClasseCreateDto;
import org.isj.hawa.groupe1.dto.ClasseUpdateDto;
import org.isj.hawa.groupe1.model.Classe;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-05-10T12:04:35+0100",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.14.1 (SAP SE)"
)
@Component
public class ClasseMapperImpl implements ClasseMapper {

    @Override
    public Classe toEntity(ClasseCreateDto dto) {
        if ( dto == null ) {
            return null;
        }

        Classe classe = new Classe();

        classe.setFilier( dto.getFilier() );
        classe.setSpecialite( dto.getSpecialite() );
        classe.setNiveau( dto.getNiveau() );

        return classe;
    }

    @Override
    public ClasseCreateDto toDto(Classe entity) {
        if ( entity == null ) {
            return null;
        }

        ClasseCreateDto classeCreateDto = new ClasseCreateDto();

        classeCreateDto.setFilier( entity.getFilier() );
        classeCreateDto.setSpecialite( entity.getSpecialite() );
        classeCreateDto.setNiveau( entity.getNiveau() );

        return classeCreateDto;
    }

    @Override
    public Classe toEntityMaj(ClasseUpdateDto dto) {
        if ( dto == null ) {
            return null;
        }

        Classe classe = new Classe();

        classe.setId( dto.getId() );
        classe.setFilier( dto.getFilier() );
        classe.setSpecialite( dto.getSpecialite() );
        classe.setNiveau( dto.getNiveau() );

        return classe;
    }

    @Override
    public ClasseUpdateDto toDtoMaj(Classe entity) {
        if ( entity == null ) {
            return null;
        }

        ClasseUpdateDto classeUpdateDto = new ClasseUpdateDto();

        classeUpdateDto.setId( entity.getId() );
        classeUpdateDto.setFilier( entity.getFilier() );
        classeUpdateDto.setSpecialite( entity.getSpecialite() );
        classeUpdateDto.setNiveau( entity.getNiveau() );

        return classeUpdateDto;
    }
}
