package org.isj.hawa.groupe1.service.mapper;

import javax.annotation.Generated;
import org.isj.hawa.groupe1.dto.UserUpdateDto;
import org.isj.hawa.groupe1.model.User;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-05-10T12:04:35+0100",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.14.1 (SAP SE)"
)
@Component
public class UserMapperImpl implements UserMapper {

    @Override
    public UserUpdateDto toDtoMaj(User entity) {
        if ( entity == null ) {
            return null;
        }

        UserUpdateDto userUpdateDto = new UserUpdateDto();

        if ( entity.getNom() != null ) {
            userUpdateDto.setNom( entity.getNom() );
        }
        if ( entity.getPrenom() != null ) {
            userUpdateDto.setPrenom( entity.getPrenom() );
        }
        if ( entity.getEmail() != null ) {
            userUpdateDto.setEmail( entity.getEmail() );
        }
        if ( entity.getTelephone() != null ) {
            userUpdateDto.setTelephone( entity.getTelephone() );
        }
        if ( entity.getGrade() != null ) {
            userUpdateDto.setGrade( entity.getGrade() );
        }

        return userUpdateDto;
    }
}
